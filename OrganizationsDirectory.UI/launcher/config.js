﻿window.InfinniUI = window.InfinniUI || {};
window.InfinniUI.config = window.InfinniUI.config || {};

// перекрываем дефолтные конфиги, лежащие в InfinniUI/app/config.js

window.InfinniUI.config.cacheMetadata = false;
// window.InfinniUI.config.disableGetCurrentUser = false;
window.InfinniUI.config.disableSignInExternalForm = false;

window.InfinniUI.config.serverUrl = 'http://localhost:9901';

window.InfinniUI.config.configId = 'OrganizationsDirectory';
window.InfinniUI.config.configName = 'Каталог предприятий';

window.InfinniUI.config.homePage = {Path: 'Common/SignInView'};

window.InfinniUI.config.disableLayoutManager = false;
