document.title = InfinniUI.config.configName;

moment.locale('ru');

(function($target/* , metadata*/, homePageMetadata) {
  var host = InfinniUI.config.serverUrl;

  InfinniUI.providerRegister.register('ObjectDataSource', InfinniUI.Providers.ObjectDataProvider);

  InfinniUI.providerRegister.register('ServerActionProvider', function() {
    return new InfinniUI.Providers.ServerActionProvider();
  });

  setTimeout(InfinniUI.LayoutManager.init.bind(InfinniUI.LayoutManager), 1000);
  InfinniUI.providerRegister.register('MetadataDataSource', function(metadataValue) {
    var $pageContent = $('body');
    for (var i = 3; i >= 0; i--) {
      setTimeout(function() {
        InfinniUI.LayoutManager.init();
      }, 500 + i * 300);
    }

    return new InfinniUI.Providers.MetadataProviderREST(new QueryConstructorMetadata(host, metadataValue));
  });

    /**
     * @description При изменении размеров окна пересчитывает высоту элементов представления
     */
  InfinniUI.AutoHeightService = (function() {
    var TIMEOUT = 40;
    var WAIT = 50;
    var resizeTimeout;

    $(window).resize(function() {
      clearTimeout(resizeTimeout);
      resizeTimeout = setTimeout(_.debounce(onWindowResize, WAIT), TIMEOUT);
    });

    /**
     * Изменение размера окна
     */
    function onWindowResize() {
      InfinniUI.LayoutManager.init();
    }
  })();

  InfinniUI.providerRegister.register('DocumentDataSource', InfinniUI.Providers.RestDataProvider);
  InfinniUI.providerRegister.register('RestDataSource', InfinniUI.Providers.RestDataProvider);

  var builder = new InfinniUI.ApplicationBuilder();
  var rootView = new SpecialApplicationView();
  var mainView;

  InfinniUI.global.messageBus.subscribe('onViewCreated', function(context, args) {
    if (args.value.openMode === 'Default') {
      window.contextApp = args.value.view;
    }
  });

  rootView.open($target);
  openHomePage()
        .done(function(viewMetadata) {
          var action = builder.buildType('OpenAction', viewMetadata, {parentView: rootView});
          action.execute();
        });

  /**
   * Открывает начальную страницу приложения
   * @return {promise} - promise
   */
  function openHomePage() {
    var defer = new $.Deferred();

    if (typeof homePageMetadata === 'string') {
      $.ajax({
        url: homePageMetadata,
        dataType: 'json'
      })
          .then(function(data) {
            defer.resolve({
              LinkView: {
                InlineView: {
                  View: data
                }
              }
            });
          }, function(jqXHR, textStatus, errorThrown) {
            console.error(textStatus);
          });
    } else {
      defer.resolve({
        LinkView: {
          AutoView: homePageMetadata
        }
      });
    }
    return defer.promise();
  }
})(
    $('body'),
    window.InfinniUI.config.homePage
);

/**
 * InfinniUI function
 */
function SpecialApplicationView() {
  var $container;

  this.isView = true;

  this.getContainer = function() {
    return this.$container;
  };

  this.open = function($el) {
    this.$container = $el;
  };

  this.getApplicationView = function() {
    return this;
  };

  this.menuIsInitialized = function() {
    this.isMenuInitialized = true;
  };

  this.getContext = function() {
    return null;
  };

  this.getView = function() {
    return null;
  };
}

/**
 * Конструктор запросов метаданных
 * @param {any} host - адрес сервера приложения
 * @param {any} metadata - запрашиваемые метаданные
 */
function QueryConstructorMetadata(host, metadata) {
  this.constructMetadataRequest = function() {
    return {
      requestUrl: host + '/metadata/Views/' + metadata.Path + '.json',
      method: 'GET'
    };
  };
}
