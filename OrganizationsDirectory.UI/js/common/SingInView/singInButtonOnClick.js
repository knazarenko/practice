/* global authorizedUserRedirect */

function CommonSingInViewSignInButtonOnClick(context, args) {
  var signInData = context.dataSources.SignInData.getSelectedItem();
  context.global.session.signInInternal(
    signInData.UserName,
    signInData.Password,    
    false,
    function(user) {
      authorizedUserRedirect(context, user);
    },
    function() {
      var messageBox = new InfinniUI.MessageBox({
        type: 'error',
        text: 'Неверное имя пользователя или пароль',
        buttons: [
          {
            name: 'OK'
          }
        ]
      });
    }
  );
}