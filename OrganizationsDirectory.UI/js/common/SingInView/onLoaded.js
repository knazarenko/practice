/* global authorizedUserRedirect */

function CommonSingInViewOnLoaded(context, args) {
  InfinniUI.user.onReady(function(user) {
    if (user) {
      authorizedUserRedirect(context, user);
    } else {
      context.dataSources.SignInData.createItem();
    }
  });
  context.dataSources.SignInData
    .setProperty('$.UserName', context.parameters.Email.getValue());
}