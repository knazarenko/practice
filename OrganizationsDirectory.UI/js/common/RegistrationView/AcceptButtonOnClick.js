/* global $ toastr authorizedUserRedirect validateGkhObligatoryFields */

function CommonRegistrationViewAcceptButtonOnClick(context, args) {
  var data = context.dataSources.RegistrationData.getSelectedItem();

  var condition = !areObligatoryFieldsFilled() ||
    !arePasswordsEqual();    
  if (condition) {
    return;
  }

  $.ajax({
    type: 'post',
    url: InfinniUI.config.serverUrl + '/CustomServices/UserManager/Registration',
    xhrFields: {
      withCredentials: true
    },
    data: JSON.stringify(data),
    beforeSend: function() {
      InfinniUI.loaderIndicator.show();
    },
    success: function(result) {
      InfinniUI.loaderIndicator.hide();
      if (result.IsValid) {
        context.global.session.signInInternal(
          data.Email,
          data.Password,
          true,
          function(user) {
            var parameters = null;          
            
            authorizedUserRedirect(context, user, parameters);
          },
          function(error) {
            toastr.error('Обратитель к системному администратору', 'Ошибка!');
            console.log(error);
          }
        );
      } else {
        result.ValidationMessage.forEach(function(message) {
          toastr.error(message.Message);
        }, this);
      }
    },
    error: function(err) {
      InfinniUI.loaderIndicator.hide();
      console.log(err);
    },
    contentType: 'application/json;charset=UTF-8'
  });

  function areObligatoryFieldsFilled() {
    var obligatoryFields = [
      {field: data.PasswordConfirm, caption: 'Повторить пароль'},
      {field: data.Password, caption: 'Пароль'},
      {field: data.Email, caption: 'E-mail'},
      {field: data.FIO, caption: 'ФИО'}      
    ];

    return validateGkhObligatoryFields(obligatoryFields);
  }

  function arePasswordsEqual() {
    var pass = data.Password;
    var passConf = data.PasswordConfirm;

    if (pass === passConf) {
      return true;
    }

    toastr.error('Пароли не совпадают');
    return false;
  }
}
