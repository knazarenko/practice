function CommonRegistrationViewOnLoaded(context, args) {
  var params = InfinniUI.UrlManager.getParams();
  var needClear = false;

  if (params.email) {
    context.dataSources.RegistrationData.setProperty('$.Email', params.email);
    context.controls.EmailTextBox.setEnabled(false);
    needClear = true;
  } else {
    context.dataSources.RegistrationData
      .setProperty('$.Email', context.parameters.Email.getValue());
  }

  if (params.invite) {
    context.dataSources.RegistrationData.setProperty('$.Invite', params.invite);
    context.controls.AgoButton.setEnabled(false);
    needClear = true;
  }

  if (needClear) {
    InfinniUI.UrlManager.clearUrlSearchPath();
  }
}