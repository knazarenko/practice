function myOrganizationsEditViewOnLoaded(context, args) {
  if (context.controls.FIOTextBox.getValue() === null) {
    context.controls.FIOTextBox.setValue(window.currentUser.Claims[2].Value);
  }
  if (context.controls.EmailTextBox.getValue() === null) {
    context.controls.EmailTextBox.setValue(window.currentUser.Claims[1].Value);
  }  
}

