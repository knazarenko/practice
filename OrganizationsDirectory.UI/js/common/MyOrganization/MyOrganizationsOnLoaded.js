function MyOrganizationsRegistrationViewOnLoaded(context, args) {

var path = InfinniUI.StringUtils.format('Organizations/ListView');
var container = InfinniUI.StringUtils.format('Content');
var openMode = InfinniUI.StringUtils.format('Container');

context.global.executeAction(context,
    {
      OpenAction: {
        LinkView: {
          AutoView: {
            Path: path,
            OpenMode: openMode,
            Container: container            
          }
        }
      }
    }
  );
}