(function(){
  console.log('ui loaded');
})();


function authorizedUserRedirect(context, user, parameters) {
  window.currentUser = user.Result || user;

  var metadata = 'MyOrganizations';
  var path = InfinniUI.StringUtils.format('Common/{0}', [metadata]);

  context.global.executeAction(context,
    {
      OpenAction: {
        LinkView: {
          AutoView: {
            Path: path,
            Parameters: parameters
          }
        }
      }
    }
  );
}

// Проверяет заполненность обязательных полей. В случае незаполненности выводит валидационное сообщение.
// obligatoryFields коллекция значений (и информации о них), которые нужно проверять на заполненность.
// Каждый элемент должен иметь поле field, в котором устанавливается значение, которое проверяется на заполненность;
// caption - заголовок элемента, из которого получено значение, поле не обязательно, при указании этого значения сообщение об ошибке валидации будет 'Поле "' + fieldCaption + '" обязательно для заполнения.'
// message - необязательное поле, содержит сообщение об ошибке валидации, если оно указано, то значение в поле caption не используется.
// Указывать message стоит для случаев, когда обязательность наличия значения опциональна, и надо указать условие в сообщении об ошибке.
function validateGkhObligatoryFields(obligatoryFields) {
  var wasError = false;

  _.forEach(obligatoryFields, function(field) {
    if (!validateGkhObligatoryField(field.field, field.caption, field.message)) {
      wasError = true;
    }
  });

  return !wasError;
}

function validateGkhObligatoryField(field, fieldCaption, message) {
  if (isCondition(field)) {
    if (fieldCaption) {
      toastr.error('Поле "' + fieldCaption + '" обязательно для заполнения.');
    } else {
      toastr.error(message);
    }

    return false;
  }

  return true;
}

function isCondition(field) {
  return field === '' ||
    /^\s+$/.test(field) ||
    field === null ||
    field === undefined ||
    (field.constructor === Array && field.length === 0);
}

function signOutRedirect(context, args) {
  context.global.session.signOut(function() {
    window.currentUser = undefined;
    context.global.executeAction(context,
      {
        OpenAction: {
          LinkView: {
            AutoView: {
              Path: 'Common/SignInView'
            }
          }
        }
      }
    );
  });
}
