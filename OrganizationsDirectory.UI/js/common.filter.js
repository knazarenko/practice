function gkhBuildFilterFromDataSource(filterDataSource) {
  var filterItems = filterDataSource.getItems();
  return gkhBuildFilterFromArray(filterItems);
}

function gkhSetFilter(context, filerDataSourceName, targetDataSourceName) {
  var filter = gkhBuildFilterFromDataSource(context.dataSources[filerDataSourceName]);
  context.dataSources[targetDataSourceName].suspendUpdate('gkhSetFilter');
  context.dataSources[targetDataSourceName].setFilter(filter);
  context.dataSources[targetDataSourceName].setPageNumber(0);
  context.dataSources[targetDataSourceName].resumeUpdate('gkhSetFilter');
}

function gkhBuildFilterFromArray(filterItems) {
  var filterSelect = filterItems.filter(function(item) {
    return item.Value !== null && item.Value !== undefined && item.Value !== '';
  });

  if (filterSelect.length === 0) {
    return null;
  }

  var filterMap = filterSelect.map(function(item) {
    return InfinniUI.StringUtils.format(item.Property, [item.Value]);
  });

  var filterTemplate = 'and({0})';

  var filter = InfinniUI.StringUtils.format(filterTemplate, [filterMap.join(',')]);

  return filter;
}
