function organizationsCityComboBoxOnValueChanged(context, args) {
  var newValue = args.newValue || {};

  var cityGuid = context.dataSources.MainDataSource
    .getProperty('$.full_address.city1_formal_name.AOGUID');
  var condition = args.newValue === null || newValue.AOGUID !== cityGuid;

  if (condition) {    
    context.controls.CityComboBox.setValueItem(null);
  }

  context.dataSources.MainDataSource
    .setProperty('$.full_address.city1_formal_name.AOGUID', newValue.AOGUID);
  context.dataSources.MainDataSource;
}