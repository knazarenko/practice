/* global validateGkhObligatoryFields */

function organizationsEditViewOnSaveClick(context, args) {
	if (validateObligatoryFields()){
		context.global.executeAction(context,
		{
			SaveAction: {
				DestinationValue: {
					Source: "MainDataSource"
				}
			}
		});
}

	function validateObligatoryFields() {
      var organization = context.dataSources.MainDataSource.getSelectedItem();
      var obligatoyFields = [
        {field: organization.Title, caption: 'Название'},
        {field: organization.FIO, caption: 'ФИО ответственного лица'},
        {field: organization.Email, caption: 'E-mail'}        
      ];
      return validateGkhObligatoryFields(obligatoyFields);
  }
}