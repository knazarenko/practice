module.exports = function(grunt) {
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');

  var appFiles = ['js/**/*.js'];

  var ownLauncher = 'launcher/';

  var infinniUIpath = 'bower_components/infinni-ui-v2/';
  var infinniUIout = infinniUIpath + 'out/';

  var output = '../public_html/';
  var targetAppJs = output + 'compiled/js/app.js';
  var targetPlatformSrc = output + 'compiled/platform/';

  grunt.initConfig({
    concat: {
      app: {
        options: {
          sourceMap: false,
          process: function(src, filepath) {
            return '//####' + filepath + '\n' + src;
          }
        },
        src: appFiles,
        dest: targetAppJs
      }
    },
    copy: {
      ownLauncher: {
        expand: true,
        cwd: ownLauncher,
        src: '**/*',
        dest: output
      },
      infinniUI: {
        expand: true,
        cwd: infinniUIout,
        src: '**/*',
        dest: targetPlatformSrc
      }
    },
    clean: {
      options: {
        force: true
      },
      src: [output]
    }
  });

  grunt.task.registerTask('build',
    function() {
      var tasks = [
        'clean',
        'copy',
        'concat'
      ];

      grunt.task.run(tasks);
    }
  );

  grunt.registerTask('default', function() {
    var tasks = [
      'build'
    ];

    grunt.task.run(tasks);
  });
};
