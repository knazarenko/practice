﻿using System.Collections.Generic;
using InfinniPlatform.Sdk.Documents;

namespace OrganizationsDirectory.ValidationSystem.Types
{
    /// <summary>
    /// Результат валидации документа
    /// </summary>
    public class ValidationResult : DocumentValidationResult
    {
        public ValidationResult()
        {
            Items = new List<DocumentValidationResultItem>();
            IsValid = true;
        }

        /// <summary>
        /// Фиксация ошибки
        /// </summary>
        /// <param name="message">Текст ошибки</param>
        public void Error(string message)
        {
            Items.Add(new DocumentValidationResultItem
            {
                Message = message
            });

            IsValid = false;
        }
    }
}
