﻿using InfinniPlatform.Sdk.Documents;

namespace OrganizationsDirectory.ValidationSystem.Types
{
    public interface IValidatior<in TDocument> : IValidatorBase where TDocument : Document
    {
        /// <summary>
        /// Функция проверки документа
        /// </summary>
        /// <param name="document">Проверяемый документ</param>
        /// <param name="validationResult">Результат проверки</param>
        void Validate(TDocument document, ValidationResult validationResult);
    }
}
