﻿namespace OrganizationsDirectory.ValidationSystem.Types
{
    /// <summary>
    /// Базовый интерфейс для регистрации валидаров.
    /// Для объявления валидатора нужно использовать интерфейс IValidator
    /// </summary>
    public interface IValidatorBase
    {
    }
}
