﻿using InfinniPlatform.Sdk.Documents;
using OrganizationsDirectory.ValidationSystem.Types;

namespace OrganizationsDirectory.ValidationSystem
{
    public interface IValidationManager
    {
        /// <summary>
        /// Проверка валидности документа
        /// </summary>
        /// <typeparam name="T">Тип документа</typeparam>
        /// <param name="document">Документ</param>
        /// <returns>Результат валидации</returns>
        ValidationResult Validate<T>(T document) where T : Document;
    }
}
