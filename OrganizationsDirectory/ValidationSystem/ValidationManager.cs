﻿using System;
using System.Collections.Generic;
using System.Linq;
using InfinniPlatform.Sdk.Documents;
using OrganizationsDirectory.ValidationSystem.Types;

namespace OrganizationsDirectory.ValidationSystem
{
    public class ValidationManager : IValidationManager
    {
        private readonly IEnumerable<IValidatorBase> _validators;

        public ValidationManager(IEnumerable<IValidatorBase> validators)
        {
            _validators = validators;
        }

        public ValidationResult Validate<T>(T document) where T : Document
        {
            var result = new ValidationResult();

            var targetValidators = GetTargetValidators<T>();

            foreach (var validatior in targetValidators)
            {
                try
                {
                    validatior.Validate(document, result);
                }
                catch (Exception e)
                {
                    result.Error(e.Message);
                }
            }

            return result;
        }

        private IEnumerable<IValidatior<T>> GetTargetValidators<T>() where T : Document
        {
            return _validators.OfType<IValidatior<T>>();
        }
    }
}
