﻿using System.Collections.Generic;

namespace OrganizationsDirectory.CustomServices
{
    public class ValidationMessage
    {
        public string Message;
    }

    public class CustomServiceResult
    {
        public bool IsValid;
        public List<ValidationMessage> ValidationMessage;
    }
}
