﻿using System.Threading.Tasks;
using InfinniPlatform.Sdk.Dynamic;
using InfinniPlatform.Sdk.Http.Services;
using OrganizationsDirectory.CustomServices.UserService;

namespace OrganizationsDirectory.CustomServices
{
    public class UserManager : IHttpService
    {
        private readonly IUserService _userService;        

        public UserManager(IUserService userService)
        {
            _userService = userService;            
        }

        public void Load(IHttpServiceBuilder builder)
        {
            builder.ServicePath = "/CustomServices/UserManager";

            builder.Post["/Registration"] = Registration;
        }

        private Task<object> Registration(IHttpRequest arg)
        {
            DynamicWrapper properties = arg.Form;

            var result = _userService.RegisterNewUser(properties);            

            return Task.FromResult<object>(result);
        }
    }
}
