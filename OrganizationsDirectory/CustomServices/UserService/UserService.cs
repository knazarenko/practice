﻿using System;
using System.Collections.Generic;
using InfinniPlatform.Sdk.Dynamic;
using InfinniPlatform.Sdk.Logging;
using InfinniPlatform.Sdk.Security;
using OrganizationsDirectory.Documents.Organization.Validators;

namespace OrganizationsDirectory.CustomServices.UserService
{
    public class UserService : IUserService
    {
        private readonly IAppUserManager _userManager;
        private readonly ILog _log;

        public UserService(IAppUserManager userManager, ILog log)                                   
        {
            _userManager = userManager;
            _log = log;            
        }

        public CustomServiceResult RegisterNewUser(DynamicWrapper properties)
        {
            var userName = properties["Name"]?.ToString() ?? Guid.NewGuid().ToString();
            var password = properties["Password"]?.ToString();
            var passwordConfirm = properties["PasswordConfirm"]?.ToString();
            var email = properties["Email"]?.ToString();

            var validationErrors = Validate(password, passwordConfirm, email);
            if (validationErrors.Count > 0)
                return GetErrorResult(validationErrors);                                    

            try
            {
                _userManager.CreateUser(userName, password, email);

                var tenant = Guid.NewGuid().ToString();

                _userManager.AddClaim(userName, "TenantId", tenant);
                _userManager.AddClaim(userName, "Email", email);
                _userManager.AddClaim(userName, "FIO", properties["FIO"]?.ToString());
            }
            catch (Exception e)
            {
                _log.Error(e.Message, e);
                return GetErrorResult(new List<ValidationMessage> { new ValidationMessage { Message = e.Message } });
            }
            return new CustomServiceResult { IsValid = true };
        }

        private static List<ValidationMessage> Validate(string password, string passwordConfirm, string email)
        {
            var errors = new List<ValidationMessage>();

            if (string.IsNullOrWhiteSpace(password))
                errors.Add(new ValidationMessage { Message = "Нужно указать пароль" });

            if (!string.IsNullOrWhiteSpace(password) && !password.Equals(passwordConfirm))
                errors.Add(new ValidationMessage { Message = "Пароль и подтверждение не совпадают" });

            if (string.IsNullOrWhiteSpace(email))
                errors.Add(new ValidationMessage { Message = "Нужно указать адрес электронной почты" });

            if (!string.IsNullOrWhiteSpace(email) && !EmailValidator.IsValidEmail(email))
                errors.Add(new ValidationMessage { Message = "Нужно указать корректный адрес электронной почты" });

            return errors;
        }
        /*private static bool IsValidEmail(string email)
        {
            try
            {
                // ReSharper disable once ObjectCreationAsStatement
                new MailAddress(email);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }*/

        private static CustomServiceResult GetErrorResult(List<ValidationMessage> errors)
        {
            return new CustomServiceResult
            {
                IsValid = false,
                ValidationMessage = errors
            };
        }
    }
}
