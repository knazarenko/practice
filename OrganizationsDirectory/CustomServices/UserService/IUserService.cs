﻿using InfinniPlatform.Sdk.Dynamic;

namespace OrganizationsDirectory.CustomServices.UserService
{
    public interface IUserService
    {
        CustomServiceResult RegisterNewUser(DynamicWrapper properties);
    }
}
