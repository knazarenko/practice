﻿using OrganizationsDirectory.ValidationSystem;

namespace OrganizationsDirectory.Documents.Organization
{
    public class Handler : DocumentHandlerBase<Organization>
    {
        public Handler(IValidationManager validationManager) : base(validationManager) { }
    }
}
