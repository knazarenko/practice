﻿using System;
using InfinniPlatform.Sdk.BlobStorage;
using InfinniPlatform.Sdk.Documents;
using OrganizationsDirectory.Types;

namespace OrganizationsDirectory.Documents.Organization
{
    [DocumentType("Organizations")]
    public class Organization : Document
    {
        /// <summary>
        /// ФИО ответственного лица
        /// </summary>
        public string FIO { get; set; }

        /// <summary>
        /// E-mail
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Сайт
        /// </summary>
        public string Site { get; set; }

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Улица, дом
        /// </summary>
        public string StreetHouse { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        public City City { get; set; }

        /// <summary>
        /// Дополнительная информация
        /// </summary>
        public string Information { get; set; }

        public BlobInfo Photo { get; set; }

        /// <summary>
        /// Город (очень некрасивая реализация)
        /// </summary>
        [Obsolete("Вместо этого поля лучше использовать City")]
        public object full_address { get; set; }
    }
}
