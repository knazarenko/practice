﻿using OrganizationsDirectory.ValidationSystem.Types;
using System.Text.RegularExpressions;
using OrganizationsDirectory.Types;





namespace OrganizationsDirectory.Documents.Organization.Validators
{
    public class SimpleValidator : IValidatior<Organization>
    {
        public void Validate(Organization document, ValidationResult validationResult)
        {
            if (string.IsNullOrWhiteSpace(document.Title))
            {
                validationResult.Error("Название предприятия не может быть пустым.");
            }
            if (!string.IsNullOrWhiteSpace(document.Site) && string.IsNullOrWhiteSpace(document.Phone))
            {
                validationResult.Error("Если введён сайт, должен быть введён и телефон.");
            }
            if (!string.IsNullOrWhiteSpace(document.StreetHouse) && document.City == null)
            { 
                validationResult.Error("Поле Улица не может заполняться, если не заполнено поле Город.");
            }
            if (!string.IsNullOrWhiteSpace(document.Information) && !Regex.IsMatch(document.Information, @"^[а-яА-Я0-9,.!?;]+$"))
            {
                validationResult.Error("Поле Информация может содержать только русские текстовые буквы, цифры и знаки препинания.");
            }
            if (string.IsNullOrWhiteSpace(document.FIO))
            {
                validationResult.Error("Имя ответственного лица не может быть пустым.");
            }
            if (!string.IsNullOrWhiteSpace(document.FIO) && !Regex.IsMatch(document.FIO, @"^[а-яА-Я]+$"))
            {
                validationResult.Error("Поле Имя ответственного лица может содержать только русские текстовые буквы.");
            }
            if (!string.IsNullOrWhiteSpace(document.Phone) && !Regex.IsMatch(document.Phone, @"^[0-9]+$"))
            {
                validationResult.Error("Поле Телефон может содержать только цифры.");
            }
            if (!string.IsNullOrWhiteSpace(document.Email) && !EmailValidator.IsValidEmail(document.Email))
            {
                validationResult.Error("Некорректный адрес email");
            }
        }


    }
}
