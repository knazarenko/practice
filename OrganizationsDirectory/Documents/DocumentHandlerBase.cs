﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InfinniPlatform.Sdk.BlobStorage;
using InfinniPlatform.Sdk.Documents;
using InfinniPlatform.Sdk.Documents.Services;
using OrganizationsDirectory.ValidationSystem;

namespace OrganizationsDirectory.Documents
{
    public class DocumentHandlerBase<TDocument> : DocumentHttpServiceHandler<TDocument> where TDocument : Document
    {
        private readonly IValidationManager _validationManager;

        public DocumentHandlerBase(IValidationManager validationManager)
        {
            _validationManager = validationManager;
        }

        public override async Task<DocumentPostQueryResult> OnBeforePost(DocumentPostQuery<TDocument> query)
        {
            var validationResult = _validationManager.Validate(query.Document);

            if (validationResult.IsValid)
            {
                return await base.OnBeforePost(query);
            }

            return new DocumentPostQueryResult
            {
                ValidationResult = validationResult,
                DocumentId = query.Document._id,
                FileInfos = new Dictionary<string, BlobInfo>(),
                Status = new DocumentUpdateResult(0, 0, 0)
            };
        }
    }
}
