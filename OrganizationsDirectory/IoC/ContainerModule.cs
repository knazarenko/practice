﻿using InfinniPlatform.Sdk.Documents.Services;
using InfinniPlatform.Sdk.Http.Services;
using InfinniPlatform.Sdk.IoC;
using OrganizationsDirectory.CustomServices.UserService;
using OrganizationsDirectory.ValidationSystem;
using OrganizationsDirectory.ValidationSystem.Types;

namespace OrganizationsDirectory.IoC
{
    public class ContainerModule : IContainerModule
    {
        public void Load(IContainerBuilder builder)
        {
            var assembly = GetType().Assembly;

            //Регистрируем http сервисы в контейнере
            builder.RegisterDocumentHttpServices(assembly);
            builder.RegisterHttpServices(assembly);

            builder.RegisterType<UserService>()
                .As<IUserService>()
                .SingleInstance();

            builder.RegisterType<ValidationManager>()
                .As<IValidationManager>()
                .SingleInstance();

            builder.RegisterAssemblyTypes(assembly, type => typeof (IValidatorBase).IsAssignableFrom(type),
                rule => rule.As<IValidatorBase>().SingleInstance());

            //Регистрируем наши коллекции в контейнере
            //builder.RegisterDocumentHttpService("Organizations");
            builder.RegisterDocumentHttpService("City");
        }
    }
}
