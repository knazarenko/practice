﻿namespace OrganizationsDirectory.Types
{
    public class City
    {
        /// <summary>
        /// Идентификатор дома по ФИАС
        /// </summary>
        public string FiasGuid { get; set; }

        /// <summary>
        /// Префикс
        /// </summary>
        public string ShortName { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string FormalName { get; set; }
               
    }
}
